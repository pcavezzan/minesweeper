package fr.cavezzan.games.minesweeper.game.impl;

import java.util.List;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cavezzan.games.minesweeper.IntegrationTestConfiguration;
import fr.cavezzan.games.minesweeper.MineSweeperCliApplication;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.impl.GameGeneratorImplITCase.GameGeneratorImplITCaseSpringBeanConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MineSweeperCliApplication.class, 
		IntegrationTestConfiguration.class, GameGeneratorImplITCaseSpringBeanConfig.class})
public class GameGeneratorImplITCase {
	
	@Autowired
	private GameGeneratorImpl generator;
	
	@Test
	public void generateShouldReturnAConfiguredGame() {
		final Game result = generator.generate();
		Assert.assertNotNull("Le générateur de jeu aurait du créer un jeu à partir des paramètres.", result);
		Assert.assertEquals("La hauteur du jeu est de 4.", Integer.valueOf(4), result.getHeight());
		Assert.assertEquals("La largeur du jeu est de 6.", Integer.valueOf(6), result.getWidth());
		Assert.assertEquals("Le jeu comporte 3 mines.", 3, result.getNbMines());
		Assert.assertEquals("Le nombre de tentative est de 0.", 0, result.getCount());
		final List<Point> grid = result.getData();
		Assert.assertEquals("La grille de valeur du jeu est une matrice 6x8.", 48, grid.size());
		// On vérifie le nombre de mine sur toute la grille (même la partie non visible car cela ne pose 
		// pas de problème fonctionnel.
		long nbMine = grid.stream().filter(p -> p.getValue() == -1).count();
		Assert.assertEquals("Le générateur a bien posé 3 mines.", 3, nbMine);
	}
	
	@Configuration
	static class GameGeneratorImplITCaseSpringBeanConfig {
		
		@Bean
		@Scope(BeanDefinition.SCOPE_PROTOTYPE)
		public Scanner inputScanner() {
			return new Scanner(
					"6" + System.lineSeparator() + 
					"4" + System.lineSeparator() +
					"3" + System.lineSeparator());
		}

	}

}
