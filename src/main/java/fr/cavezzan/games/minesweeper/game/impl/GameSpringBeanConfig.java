package fr.cavezzan.games.minesweeper.game.impl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.cavezzan.games.minesweeper.Randomizer;
import fr.cavezzan.games.minesweeper.game.GameValidator;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.io.InputReader;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

/**
 * Configuration Spring des implémentations liées à la partie du jeu du démineurs.
 */
@Configuration
public class GameSpringBeanConfig {

	@Bean
	public MineSweeperImpl mineSweeperImpl(final Randomizer randomizer) {
		final MineSweeperImpl mineSweeper = new MineSweeperImpl();
		// On dénombre les mines autour de nous.
		mineSweeper.setTipsNeighboorsPoints(new TipsNeighboorsPoints());
		// On rend visible les points qui ne sont pas des mines autour de nous. 
		mineSweeper.setRevealNeighboorPoints(new RevealNeighboorPoints());
		mineSweeper.setRandomizer(randomizer);
		return mineSweeper;
	}
	
	@Bean
	public GameGeneratorImpl gameGeneratorImpl(final InputReader reader, 
			final OutputWriter writer, final MineSweeper mineSweeper) {
		final GameGeneratorImpl gameGeneratorImpl = new GameGeneratorImpl();
		gameGeneratorImpl.setInputReader(reader);
		gameGeneratorImpl.setOutputWriter(writer);
		gameGeneratorImpl.setMineSweeper(mineSweeper);
		return gameGeneratorImpl;
	}
	
	@Bean
	public PositionGameValidatorImpl positionGameValidatorImpl(final OutputWriter writer) {
		final PositionGameValidatorImpl validationImpl = new PositionGameValidatorImpl();
		validationImpl.setOutputWriter(writer);
		return validationImpl;
	}
	
	@Bean
	public CliGameRenderer cliGameRendererImpl(final OutputWriter writer) {
		final CliGameRenderer renderer = new CliGameRenderer();
		renderer.setWriter(writer);
		return renderer;
	}
	
	@Bean
	public GameManagerImpl gameManagerImpl(final MineSweeper mineSweeper,
			final InputReader reader, final GameValidator gameValidator,
			final OutputWriter writer) {
		final GameManagerImpl gameManager = new GameManagerImpl();
		gameManager.setInputReader(reader);
		gameManager.setOutputWriter(writer);
		gameManager.setGameRenderer(cliGameRendererImpl(writer));
		gameManager.setMineSweeper(mineSweeper);
		gameManager.setGameValidator(gameValidator);
		return gameManager;
	}
	
}
