package fr.cavezzan.games.minesweeper.game.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cavezzan.games.minesweeper.IntegrationTestConfiguration;
import fr.cavezzan.games.minesweeper.MineSweeperCliApplication;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.Position;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MineSweeperCliApplication.class, IntegrationTestConfiguration.class})
public class MineSweeperImplITCase {
	
	@Autowired
	private MineSweeperImpl mineSweeper;
	
	@Test
	public void minesShouldReturnTenPositions() {
		final Game game = Game.of(8, 8, 10);
		final List<Position> result = mineSweeper.mines(game);
		Assert.assertNotNull("Le mineur a normalement placé des mines.", result);
		Assert.assertFalse("Le mineur doit nous retourner une liste de position remplie.", result.isEmpty());
		Assert.assertEquals("La mineur doit avoir miné 10 positions.", 10, result.size());
	}
	
	@Test
	public void revealPositionShouldReturnTrueAndDisplayItIfItIsAMine() {
		final Game game = Game.of(3, 3, 2);
		game.setPosition(Position.of(2, 3));
		game.getData().get(13).setValue(-1);
		
		final boolean result = mineSweeper.revealPosition(game);
		
		Assert.assertTrue("La révélation d'une position minée doit renvoyer vrai.", result);
		Assert.assertTrue("La position minée doit devenir visible", game.getData().get(13).isVisible());
	}
	
	@Test
	public void revealPositionShouldReturnFalseIfItIsNotAMine() {
		final Game game = Game.of(3, 3, 2);
		game.setPosition(Position.of(2, 3));
		game.getData().get(11).setValue(-1);
		
		final boolean result = mineSweeper.revealPosition(game);
		
		Assert.assertFalse("La révélation d'une position non minée doit renvoyer false.", result);
	}

	@Test
	public void winShouldReturnFalse() {
		final Game game = Game.of(2, 2, 1);
		final boolean result = mineSweeper.win(game);
		Assert.assertFalse("Le jeu n'est pas gagnée.", result);
	}
	
	@Test
	public void winShouldReturnTrue() {
		final Game game = Game.of(2, 2, 1);
		final List<Point> grid = game.getData();
		grid.get(6).setVisible(true);
		grid.get(9).setVisible(true);
		grid.get(10).setVisible(true);
		
		final boolean result = mineSweeper.win(game);
		
		Assert.assertTrue("Le jeu est pas gagnée.", result);
	}

	
}
