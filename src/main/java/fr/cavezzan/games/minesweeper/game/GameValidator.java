package fr.cavezzan.games.minesweeper.game;

/**
 * Validateur d'une partie de jeu.
 *	
 * <p>
 * 	Permet de vérifier si la partie en cours respecte des règles définies.
 * </p>
 */
@FunctionalInterface
public interface GameValidator {
	
	boolean isValid(Game game);
	
}
