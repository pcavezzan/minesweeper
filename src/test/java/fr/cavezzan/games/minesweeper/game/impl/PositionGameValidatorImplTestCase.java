package fr.cavezzan.games.minesweeper.game.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

@RunWith(MockitoJUnitRunner.class)
public class PositionGameValidatorImplTestCase {
	
	@Mock
	private OutputWriter outputWriter;
	
	@InjectMocks
	private PositionGameValidatorImpl validator;

	@Test
	public void isValidShouldReturnTrueWhenPlayerHasChoosenCoordinatesOnGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(2, 2));
		final boolean result = validator.isValid(game);
		Assert.assertTrue("Quand le joueur choisi une case de grille du jeu, sa position est valide.", result);
	}
	
	@Test
	public void isValidShouldReturnFalseWhenPlayerHasChoosenCoordinatesOutsideTheGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(3, 3));
		final boolean result = validator.isValid(game);
		Assert.assertFalse("Quand le joueur choisi une case en dehors de la grille du jeu, sa position est valide.", 
			result);
	}
	
	@Test
	public void isValidShouldReturnFalseWhenPlayerHasChoosenOneCoordinateOutsideTheGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(1, 3));
		boolean result = validator.isValid(game);
		Assert.assertFalse(
			"Quand le joueur choisi une coordonnée en dehors de la grille du jeu, sa position est valide.", result);
		game.setPosition(Position.of(3, 1));
		result = validator.isValid(game);
		Assert.assertFalse(
			"Quand le joueur choisi une coordonnée en dehors de la grille du jeu, sa position est valide.", result);
	}

}
