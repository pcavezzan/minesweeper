package fr.cavezzan.games.minesweeper.game.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameManager.GameStatus;
import fr.cavezzan.games.minesweeper.game.GameRenderer;
import fr.cavezzan.games.minesweeper.game.GameValidator;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.io.InputReader;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

@RunWith(MockitoJUnitRunner.class)
public class GameManagerImplTestCase {
	
	@Mock
	private GameValidator gameValidator;
	
	@Mock
	private GameRenderer renderer;
	
	@Mock
	private InputReader inputReader;
	
	@Mock
	private OutputWriter outputWriter;
	
	@Mock
	private MineSweeper mineSweeper;

	@InjectMocks
	private GameManagerImpl manager;
	
	@Before
	public void setUp() {
		Mockito.when(inputReader.read("Line: ")).thenReturn(2);
		Mockito.when(inputReader.read("Column: ")).thenReturn(1);
		final Game fixtureGame = createFixtureGame();
		fixtureGame.setPosition(Position.of(2, 1));
		Mockito.when(gameValidator.isValid(Mockito.eq(fixtureGame))).thenReturn(Boolean.TRUE);
	}
	
	@Test
	public void nextPositionShouldDisplayTheGame() {
		final Game game = createFixtureGame();
		
		manager.nextPosition(game);
		
		Mockito.verify(renderer, Mockito.description("Le manager doit afficher le jeu.")).render(game);
	}
	
	@Test
	public void nextPositionShouldSetPlayerChoosenPosition() {
		final Game game = createFixtureGame();
		Assert.assertNull("Un nouveau jeu n'a pas encore de position choisie par le joueur.", game.getPosition());
		
		final Position position = manager.nextPosition(game);
		
		Assert.assertNotNull("Le manager doit renvoyer la case choisie par le joueur.", position);
		Assert.assertNotNull("Le manager doit positionner la case choisie par le joueur.", game.getPosition());
		Assert.assertEquals("La case choisie par le joueur doit être celle positionner sur la partie par le manager.", 
				position, game.getPosition());
		Assert.assertEquals("Le joueur a choisie la ligne 2.", 2, game.getPosition().getY());
		Assert.assertEquals("Le joueur a choisie la colonne 1.", 1, game.getPosition().getX());
	}

	
	@Test
	public void playShouldReturnContinueWhenNoMineFoundOrNoMorePointToReveal() {
		final Game game = createFixtureGame();
		
		final GameStatus result = manager.play(game);

		Assert.assertNotNull("Le jeu doit renvoyer un résultat.", result);
		Assert.assertEquals(
			"Le jeu doit continuer si la position joué n'est pas une mine et des cases restent à découvrir", 
			GameStatus.PLAY, result);
	}
	
	@Test
	public void playShouldReturnLoseWhenAMineIsChoosen() {
		final Game game = createFixtureGame();
		Mockito.when(mineSweeper.revealPosition(game)).thenReturn(Boolean.TRUE);
		
		final GameStatus result = manager.play(game);

		Assert.assertNotNull("Le jeu doit renvoyer un résultat.", result);
		Assert.assertEquals(
			"Le jeu a perdu car la position joué est une mine.", 
			GameStatus.LOSE, result);
	}
	
	@Test
	public void playShouldReturnWinWhenNoMineFoundOrNoMorePointToReveal() {
		final Game game = createFixtureGame();
		Mockito.when(mineSweeper.win(game)).thenReturn(Boolean.TRUE);
		
		final GameStatus result = manager.play(game);

		Assert.assertNotNull("Le jeu doit renvoyer un résultat.", result);
		Assert.assertEquals(
			"Le jeu a gagné car il ne reste que des mines sur la grille de jeu.", 
			GameStatus.WIN, result);
	}
	
	@Test
	public void nextPositionShouldDisplayAttemptNumber() {
		final Game game = createFixtureGame();
		
		manager.nextPosition(game);
		
		Mockito.verify(outputWriter,  Mockito.description("Le manager doit afficher le nombre de tentative de jeu."))
		.writeLine(ArgumentMatchers.argThat((arg) -> arg.equals("Attempt Number: 1")));
	}

	@Test
	public void playShouldDisplayYouLoseAndTheMineSweeper() {
		final Game game = createFixtureGame();
		Mockito.when(mineSweeper.revealPosition(game)).thenReturn(Boolean.TRUE);
		
		manager.play(game);
		
		Mockito.verify(outputWriter,  Mockito.description("Le manager doit afficher à l'utilisateur qu'il a perdu."))
		.writeLine(ArgumentMatchers.argThat((arg) -> arg.equals("Sorry, you loose !")));
		Mockito.verify(renderer,  Mockito.description("Le manager doit afficher à l'utilisateur la partie perdante."))
			.render(game);
		
	}

	@Test
	public void playShouldDisplayYouWinAndTheMineSweeper() {
		final Game game = createFixtureGame();
		Mockito.when(mineSweeper.win(game)).thenReturn(Boolean.TRUE);
		
		manager.play(game);
		
		Mockito.verify(outputWriter, Mockito.description("Le manager doit afficher à l'utilisateur qu'il a gagné."))
		.writeLine(ArgumentMatchers.argThat((arg) -> arg.equals("Congratulation, you win in 1 attempts !")));
		Mockito.verify(renderer,  Mockito.description("Le manager doit afficher à l'utilisateur la partie gagnante."))
		.render(game);
	}
	
	private Game createFixtureGame() {
		final Game game = Game.of(4, 4, 2);
		game.setCount(1);
		return game;
	}

}
