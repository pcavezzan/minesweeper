package fr.cavezzan.games.minesweeper.game.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.io.InputReader;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

@RunWith(MockitoJUnitRunner.class)
public class GameGeneratorImplTestCase {
	
	@Mock
	private InputReader reader;
	
	@Mock
	private OutputWriter writer;
	
	@Mock
	private MineSweeper mineSweeper;
	
	@InjectMocks
	private GameGeneratorImpl gameGeneratorImpl;
	
	@Test
	public void generateShouldReturnAGameConfigured() {
		Mockito.when(reader.read(GameGeneratorImpl.GAME_WIDTH_PARAMNAME)).thenReturn(6);
		Mockito.when(reader.read(GameGeneratorImpl.GAME_HEIGHT_PARAMNAME)).thenReturn(4);
		Mockito.when(reader.read(GameGeneratorImpl.NUMBER_OF_MINES_PARAMNAME)).thenReturn(3);

		final Game result = gameGeneratorImpl.generate();
		
		Mockito.verify(mineSweeper, Mockito.description("Le démineur aurait du miner la grille de jeu.")).mines(Mockito.any());
		Mockito.verify(mineSweeper, Mockito.description("Le démineur aurait placer les indices.")).tips(Mockito.any());
		
		Assert.assertNotNull("Le générateur de jeu aurait du créer un jeu à partir des paramètres.", result);
		Assert.assertEquals("La hauteur du jeu est de 6.", Integer.valueOf(4), result.getHeight());
		Assert.assertEquals("La largeur du jeu est de 4.", Integer.valueOf(6), result.getWidth());
		Assert.assertEquals("Le jeu comporte 3 mines.", 3, result.getNbMines());
		Assert.assertEquals("Le nombre de tentative est de 0.", 0, result.getCount());
		Assert.assertNotNull("Le jeu doit contenir les données de la grille.", result.getData());
		Assert.assertFalse("Le jeu doit contenir les données de la grille.", result.getData().isEmpty());
		Assert.assertEquals("La grille de jeu est composée de 48 cases (dont 24 invisibles).", 48, result.getData().size());
	}
	
}
