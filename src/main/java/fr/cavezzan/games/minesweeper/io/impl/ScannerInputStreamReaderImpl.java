package fr.cavezzan.games.minesweeper.io.impl;

import java.util.InputMismatchException;
import java.util.Scanner;

import fr.cavezzan.games.minesweeper.io.InputReader;

/**
 * Implémentation de la lecture des paramètres saisie par l'utilisateur 
 * à l'aide d'un scanner d'entrée.
 */
class ScannerInputStreamReaderImpl implements InputReader {
	
	/** Le scanner d'entrée. */
	private Scanner input;

	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.InputReader#read(java.lang.String)
	 */
	@Override
	public int read(final String parameter) {
		Integer value = null;
		while (value == null) {
			try {
				System.out.print(parameter);
				value = Integer.valueOf(input.nextInt());
			} catch (final InputMismatchException wrongParameterEx) {
				System.out.println("Please, select a number.");
				input.nextLine();
			}
		} 
		
		return value;
	}
	
	public void setInput(final Scanner input) {
		this.input = input;
	}

	public Scanner getInput() {
		return input;
	}

}
