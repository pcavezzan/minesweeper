package fr.cavezzan.games.minesweeper;

public interface Randomizer {
	
	int random(int bound);
	
}
