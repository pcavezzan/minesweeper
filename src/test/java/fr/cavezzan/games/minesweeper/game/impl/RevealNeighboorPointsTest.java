package fr.cavezzan.games.minesweeper.game.impl;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Position;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RevealNeighboorPointsTest {

	private final RevealNeighboorPoints revealNeighboorPoints = new RevealNeighboorPoints();

	/*
	Use case:
		0x    0x    0x    0x
		0x    0x    0x    0x
		0x    0x    0x    0x
		0x    0x    0x    0x
	 */
	@Test
	public void applyShouldRevealAllPointsInGameWhenNoMines() {
		final Game game = Game.of(4, 4, 0);
		final int result = revealNeighboorPoints.apply(game, Position.of(2,2));
		Assert.assertEquals(16, result);
	}


	@Test
	public void applyShouldRevealAllNeighboorAdjacentPointsUntilOneIsSurroundedWhenNotSurroundedByAMine() {
		/*
		Use case:
		1      1    1x    0x
		1     -1    1x    0x
		1x    1x    1x    0x
		0x    0x    0x    0x
	 	*/
		Game game = Game.of(4, 4, 1);

		game.getPointAtPosition(Position.of(2, 2)).setValue(MineSweeperImpl.MINE);
		game.getPointAtPosition(Position.of(2, 1)).setValue(1);
		game.getPointAtPosition(Position.of(2, 3)).setValue(1);
		game.getPointAtPosition(Position.of(1, 2)).setValue(1);
		game.getPointAtPosition(Position.of(1, 1)).setValue(1);
		game.getPointAtPosition(Position.of(1, 3)).setValue(1);
		game.getPointAtPosition(Position.of(3, 2)).setValue(1);
		game.getPointAtPosition(Position.of(3, 1)).setValue(1);
		game.getPointAtPosition(Position.of(3, 3)).setValue(1);

		int result = revealNeighboorPoints.apply(game, Position.of(4, 4));
		Assert.assertEquals(12, result);

		/*
		Use case:
		1      1x     1x     0x
		1      -1     1x     0x
		1x     1x     1x     0x
		0x     0x     0x     0x
		*/
		game = Game.of(4, 4, 1);

		game.getPointAtPosition(Position.of(2, 2)).setValue(MineSweeperImpl.MINE);
		game.getPointAtPosition(Position.of(2, 1)).setValue(1);
		game.getPointAtPosition(Position.of(2, 3)).setValue(1);
		game.getPointAtPosition(Position.of(1, 2)).setValue(1);
		game.getPointAtPosition(Position.of(1, 1)).setValue(1);
		game.getPointAtPosition(Position.of(1, 3)).setValue(1);
		game.getPointAtPosition(Position.of(3, 2)).setValue(1);
		game.getPointAtPosition(Position.of(3, 1)).setValue(1);
		game.getPointAtPosition(Position.of(3, 3)).setValue(1);

		result = revealNeighboorPoints.apply(game, Position.of(1, 3));
		Assert.assertEquals(13, result);
	}

}