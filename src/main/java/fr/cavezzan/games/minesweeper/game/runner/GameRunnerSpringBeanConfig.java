package fr.cavezzan.games.minesweeper.game.runner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.cavezzan.games.minesweeper.game.GameGenerator;
import fr.cavezzan.games.minesweeper.game.GameManager;

/**
 * Configuration Spring des implémentations liées à la partie au lanceur du jeu.
 */
@Configuration
public class GameRunnerSpringBeanConfig {

	@Bean
	public CliGameRunner gameRunner(GameGenerator gameGenerator, GameManager gameManager) {
		final CliGameRunner gameRunner = new CliGameRunner();
		gameRunner.setGenerator(gameGenerator);
		gameRunner.setManager(gameManager);
		return gameRunner;
	}
	
}
