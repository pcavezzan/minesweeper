package fr.cavezzan.games.minesweeper.game.impl;

import org.junit.Assert;
import org.junit.Test;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Position;

public class TipsNeighboorsPointsTestCase {

	private final TipsNeighboorsPoints function = new TipsNeighboorsPoints();
	
	@Test
	public void applyShouldReturnNumberOfSurrondedMines() {
		final Game game = Game.of(2, 2, 1);
		game.getData().get(5).setValue(-1);
		game.getData().get(10).setValue(-1);
		
		Assert.assertEquals(
			"Le point est entouré de 2 mines.", 
			Integer.valueOf(2), 
			function.apply(game, Position.of(1, 2))
		);
	}
	
}
