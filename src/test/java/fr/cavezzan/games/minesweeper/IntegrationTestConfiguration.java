package fr.cavezzan.games.minesweeper;

import java.util.Scanner;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class IntegrationTestConfiguration {
	
	@Bean
	public CommandLineRunner gameRunner() {
		return (args) -> {
		};
	}
	
	@Bean
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	public Scanner inputScanner() {
		return new Scanner(
				"");
	}
	
}
