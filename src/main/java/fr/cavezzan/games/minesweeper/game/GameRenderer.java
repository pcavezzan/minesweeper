package fr.cavezzan.games.minesweeper.game;


/**
 * Composant de rendu d'un jeu.
 */
public interface GameRenderer {
	
	
	/**
	 * Affiche le jeu à l'utilisateur.
	 * 
	 * @param game La partie à afficher.
	 * 
	 * @return Le nombre de case dévoilé.
	 */
	Integer render(Game game);
	
}
