package fr.cavezzan.games.minesweeper.game.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameGenerator;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.io.InputReader;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

/**
 * Implémentation du générateur d'une partie de démineur.
 *	
 * <p>
 * 	Le démineur est composé d'une grille (matrix nxm) dont une partie seulement est visible (n-2)x(m-2).
 * 	La partie visible de la grille est dictée par les paramètres de la partie.<br/>
 * 	Une fois la grille générée, on crée l'instance de notre partie, puis on laisse le démineur (stratégie)
 * 	le soin de
 * </p>
 * 
 * <ol>
 * 	<li>minée la grille visible.</li>
 * 	<li>placée les indications du nombre de mines entourant chaque point non miné de la grille,</li>
 * </ol>
 */
class GameGeneratorImpl implements GameGenerator {
	
	static final String NUMBER_OF_MINES_PARAMNAME = "Number of Mines: ";
	static final String GAME_HEIGHT_PARAMNAME = "Height: ";
	static final String GAME_WIDTH_PARAMNAME = "Width: ";

	/** Le logger. */
	private Logger LOG = LoggerFactory.getLogger(GameGeneratorImpl.class);
	
	private InputReader inputReader;
	
	private OutputWriter outputWriter;
	
	/** La stratégie de déminage, le démineur. */
	private MineSweeper mineSweeper; 
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.GameGenerator#generate()
	 */
	@Override
	public Game generate() {
		outputWriter.writeLine("Please select your game parameter.");
		int width = inputReader.read(GAME_WIDTH_PARAMNAME);
		int height = inputReader.read(GAME_HEIGHT_PARAMNAME);
		int numberOfMines = inputReader.read(NUMBER_OF_MINES_PARAMNAME);
	
		// Création du jeu (les cases)
		final Game game = Game.of(width, height, numberOfMines);
		LOG.info("Game ({}) has been created, setting up mines.", game);
		// Place les mines 
		mineSweeper.mines(game);
		// Place les tips
		LOG.info("Placing the tips on grid.");
		mineSweeper.tips(game);
		return game;
	}
	
	public void setInputReader(InputReader inputReader) {
		this.inputReader = inputReader;
	}
	
	public InputReader getInputReader() {
		return inputReader;
	}
	
	public void setOutputWriter(OutputWriter outputWriter) {
		this.outputWriter = outputWriter;
	}
	
	public Logger getLOG() {
		return LOG;
	}
	
	public void setMineSweeper(MineSweeper mineSweeper) {
		this.mineSweeper = mineSweeper;
	}
	
	public MineSweeper getMineSweeper() {
		return mineSweeper;
	}

}
