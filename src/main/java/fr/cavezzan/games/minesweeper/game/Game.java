package fr.cavezzan.games.minesweeper.game;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Représente une partie d'un jeu de démineur.
 * 
 * <p>
 * 	Le jeu du démineur possêde les caractéristiques suivantes: grilles de points, dimensions, nombre de mines.<br/>
 * 	A cela, on rajoute la position choisie de l'utilisateur et le nombre de coup joué.
 * </p>
 */
public class Game {
	
	private final List<Point> data;
	private final int width, height, nbMines;
	private int count;
	private Position position;
	
	public static Game of(final int width, final int height, final int nbMines) {
		return new Game(width, height, nbMines);
	}
	
	private Game(final int width, final int height, final int nbMines) {
		this.nbMines = nbMines;
		this.width = width;
		this.height = height;
		int w = width + 2;
		int h = height + 2;
		final Point[] points = new Point[h*w];
		Arrays.setAll(points, i -> new Point(Position.of(i / w, i % w)));
		this.data = Collections.unmodifiableList(Arrays.asList(points));
	}

	public int getNbMines() {
		return nbMines;
	}

	public List<Point> getData() {
		return data;
	}
	
	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}

	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	public Position getPosition() {
		return position;
	}

	public Integer getHeight() {
		return height;
	}
	
	public Integer getWidth() {
		return width;
	}

	private boolean isValidPosition(Position position) {
		return position.getX() >= 0 && position.getX() < height + 2
				&& position.getY() >= 0 && position.getY() <= width + 2;
	}

	public boolean isDisplayInGridPosition(Position position) {
		return position.getX() > 0 && position.getX() <= height
				&& position.getY() > 0 && position.getY() <= width;
	}

	public Point getPointAtPosition(Position position) throws IllegalArgumentException {
		if (!isValidPosition(position)) {
			throw new IllegalArgumentException("Position requested out of grid.");
		}
		final int index = position.getX() + (position.getY() * (width + 2));
		return data.get(index);
	}
	
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Game [data=");
		builder.append(data);
		builder.append(", width=");
		builder.append(width);
		builder.append(", height=");
		builder.append(height);
		builder.append(", nbMines=");
		builder.append(nbMines);
		builder.append(", count=");
		builder.append(count);
		builder.append(", position=");
		builder.append(position);
		builder.append("]");
		return builder.toString();
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + height;
		result = prime * result + nbMines;
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + width;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (count != other.count)
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (height != other.height)
			return false;
		if (nbMines != other.nbMines)
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return width == other.width;
	}


	/**
	 * Représente un point visitable de la grille de jeu.
	 * <p>
	 * 	Un point peut être caché ou visible et possède le nombre de mine l'entourant.
	 * </p>
	 */
	public static class Point {
		private int value;
		private boolean visible;
		private final Position position;

		Point(final Position p) {
			this.position = p;
		}
		
		public Position getPosition() {
			return position;
		}

		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}
		
		public boolean isHidden() {
			return !isVisible();
		}

		public boolean isVisible() {
			return visible;
		}

		public void setVisible(boolean visible) {
			this.visible = visible;
		}

		@Override
		public String toString() {
			final StringBuilder builder = new StringBuilder();
			builder.append("Point [value=");
			builder.append(value);
			builder.append(", visible=");
			builder.append(visible);
			builder.append(", position=");
			builder.append(position);
			builder.append("]");
			return builder.toString();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((position == null) ? 0 : position.hashCode());
			result = prime * result + value;
			result = prime * result + (visible ? 1231 : 1237);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			if (position == null) {
				if (other.position != null)
					return false;
			} else if (!position.equals(other.position))
				return false;
			if (value != other.value)
				return false;
			return visible == other.visible;
		}
		
		
	}
}
