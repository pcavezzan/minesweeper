package fr.cavezzan.games.minesweeper.game.impl;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.Position;

/**
 * La stratégie permettant de dénombrer l'ensemble des points minés adjacents à une position.
 */
class TipsNeighboorsPoints implements BiFunction<Game, Position, Integer> {

	@Override
	public Integer apply(final Game game, final Position position) {
		final List<Point> data = game.getData();
		Integer result = 0;
		
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				final Position pos = Position.of(position.getY() + i, position.getX() + j);
				final int index = pos.getX() + (pos.getY() * (game.getWidth() + 2));
				final Point neightboorPoint = data.get(index);
				if (MineSweeperImpl.MINE_P.test(neightboorPoint)) {
					result++;					
				}
			}
		}
		
		return result;
	}
}
