package fr.cavezzan.games.minesweeper.game;

/**
 * Indique une position au sein de la grille de jeu.
 * 
 * <p>
 * 	Une position est une simple coordonnée à 2 dimensions (x, y). 
 * </p>
 */
public class Position {
	private final int x;
	private final int y;
	
	private Position(int y, int x) {
		this.y = y;
		this.x = x;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public static final Position of(final int y, final int x) {
		return new Position(y, x);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Position [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append(']');
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}