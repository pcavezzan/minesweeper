package fr.cavezzan.games.minesweeper.game;

import org.junit.Assert;
import org.junit.Test;

public class PositionTestCase {
	
	@Test
	public void of() {
		final Position position = Position.of(4, 3);
		Assert.assertEquals("La position doit être placée sur la 4e ligne.", 4, position.getY());
		Assert.assertEquals("La position doit être placée sur la 3e colonne.", 3, position.getX());
	}
	
}
