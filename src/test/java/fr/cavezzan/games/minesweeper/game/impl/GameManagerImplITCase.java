package fr.cavezzan.games.minesweeper.game.impl;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cavezzan.games.minesweeper.IntegrationTestConfiguration;
import fr.cavezzan.games.minesweeper.MineSweeperCliApplication;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.game.GameManager.GameStatus;
import fr.cavezzan.games.minesweeper.game.impl.GameManagerImplITCase.GameManagerImplITCaseSpringBeanConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MineSweeperCliApplication.class, IntegrationTestConfiguration.class,
		GameManagerImplITCaseSpringBeanConfig.class})
public class GameManagerImplITCase {
	
	@Autowired
	private GameManagerImpl manager;
	
	@Test
	public void play() {
		final Game game = Game.of(8, 8, 10);
		game.setPosition(Position.of(4, 4));
		final GameStatus result = manager.play(game);
		Assert.assertNotNull("Le manager a renvoyé un résultat après un coup de joué.", result);
	}
	
	@Configuration
	static class GameManagerImplITCaseSpringBeanConfig {
		
		@Bean
		@Scope(BeanDefinition.SCOPE_PROTOTYPE)
		public Scanner inputScanner() {
			return new Scanner(
					"8" + System.lineSeparator() + 
					"8" + System.lineSeparator() +
					"10" + System.lineSeparator());
		}

	}

	
}
