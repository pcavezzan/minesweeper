package fr.cavezzan.games.minesweeper.game.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameGenerator;
import fr.cavezzan.games.minesweeper.game.GameManager;
import fr.cavezzan.games.minesweeper.game.GameManager.GameStatus;

/**
 * Lanceur du démineur en ligne de commande.
 * 
 * <p>
 * 	Le principe du jeu est simple:
 * </p>
 * 
 * <ol>
 *  <li>
 *  	On génère notre partie en fonction d'une stratégie.
 *  	Cette dernière s'appuie sur la saisie des paramètres du jeu: width, height & numberOfMines.
 *  </li>
 *  <li>Tant que la partie n'est pas finie, on joue.</li>
 * </ol>
 *
 */
public class CliGameRunner implements CommandLineRunner {
	
	/** Le logger. */
	private static final Logger LOG = LoggerFactory.getLogger(CliGameRunner.class);
	
	/** Le générateur de partie de jeu. */
	private GameGenerator generator;
	
	/** Le gestionnaire d'une partie. */
	private GameManager manager;

	/* (non-Javadoc)
	 * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
	 */
	@Override
	public void run(final String... args) throws Exception {
		final Game game = generator.generate();
		LOG.debug("The game will be : {}", game);
		GameStatus result = GameStatus.PLAY;
		while (result == GameStatus.PLAY) {
			int attemptNumber = game.getCount();
			game.setCount(++attemptNumber);
			manager.nextPosition(game);
			result = manager.play(game);
		}
	}
	
	public GameGenerator getGenerator() {
		return generator;
	}
	
	public void setGenerator(GameGenerator generator) {
		this.generator = generator;
	}

	public GameManager getManager() {
		return manager;
	}
	
	public void setManager(final GameManager manager) {
		this.manager = manager;
	}
}
