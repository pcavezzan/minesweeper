package fr.cavezzan.games.minesweeper.game.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameManager;
import fr.cavezzan.games.minesweeper.game.GameRenderer;
import fr.cavezzan.games.minesweeper.game.GameValidator;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.io.InputReader;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

/**
 * Implémentation d'un gestionnaire de partie.
 * 
 * <p>
 * 	La gestion d'une partie suit la logique suivante:
 * 	<ol>
 * 		<li>J'affiche la grille de jeu à l'utilisateur,</li>
 * 		<li>Je demande la position où l'utilisateur souhaite se déplacer,</li>
 * 		<li>Je déplace la position de l'utilisateur,</li>
 * 		<li>
 * 			Je découvre si la case est une mine ou pas,
 * 			<ul>
 * 				<li>Si c'est une mine, la partie est finie et j'affiche la grille à l'utilisateur,</li>
 * 				<li>Si ce n'est pas une mine, j'affiche ma position et celle de mes voisins non minés,</li>
 * 				<li>S'il ne reste que des mines en position cachée, l'utilisateur a gagné !</li>
 * 			</ul>
 * 		</li>
 * 	</ol>
 * </p>
 */
class GameManagerImpl implements GameManager {
	
	/** Le logger. */
	private static final Logger LOG = LoggerFactory.getLogger(GameManagerImpl.class);
	
	private GameRenderer gameRenderer;

	/** Effectue une validation de la partie en cours. */
	private GameValidator gameValidator;
	
	/** Récupère les entrée utilisateur. */
	private InputReader inputReader;
	
	/** Récupère l'écrivain sur la sortie utilisateur */
	private OutputWriter outputWriter;
	
	/** La stratégie de déminage, la logique du démineur. */
	private MineSweeper mineSweeper;

	/*
	 * (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.GameManager#nextPosition(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public Position nextPosition(Game game) {
		final int attemptNumber = game.getCount();
		LOG.debug("Attempt number --> {}", attemptNumber);
		gameRenderer.render(game);
		do {
			game.setPosition(null);
			LOG.debug("Getting player next move.");
			outputWriter.writeLine("Attempt Number: " + attemptNumber);
			final int line = inputReader.read("Line: ");
			final int column = inputReader.read("Column: ");
			game.setPosition(Position.of(line, column));
		} while(!gameValidator.isValid(game));
		
		return game.getPosition(); 
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.GameManager#play(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public GameStatus play(final Game game) {
		GameStatus result = GameStatus.PLAY;
		if (mineSweeper.revealPosition(game)) {
			result = GameStatus.LOSE;
			LOG.debug("Player has choosen a mine ! Game over ! He's just lost ...");
			gameRenderer.render(game);
			outputWriter.writeLine("Sorry, you loose !");
		} else if (mineSweeper.win(game)) {
			result = GameStatus.WIN;
			gameRenderer.render(game);
			LOG.debug("Player has discovered all mines ! Game over ! He's just won ...");
			outputWriter.writeLine("Congratulation, you win in " + game.getCount() + " attempts !");
		}
		return result;
	}
	
	public GameValidator getGameValidator() {
		return gameValidator;
	}
	
	public void setGameValidator(GameValidator gameValidator) {
		this.gameValidator = gameValidator;
	}
	
	public InputReader getInputReader() {
		return inputReader;
	}
	
	public void setInputReader(InputReader inputReader) {
		this.inputReader = inputReader;
	}
	
	public MineSweeper getMineSweeper() {
		return mineSweeper;
	}
	
	public void setMineSweeper(MineSweeper mineSweeper) {
		this.mineSweeper = mineSweeper;
	}
	
	public void setOutputWriter(OutputWriter outputWriter) {
		this.outputWriter = outputWriter;
	}
	
	public OutputWriter getOutputWriter() {
		return outputWriter;
	}
	
	public GameRenderer getGameRenderer() {
		return gameRenderer;
	}
	
	public void setGameRenderer(GameRenderer gameRenderer) {
		this.gameRenderer = gameRenderer;
	}
}
